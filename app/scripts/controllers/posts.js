'use strict';

app.controller('PostsCtrl', function ($scope, $location, Post, Auth){
	$scope.post = {text: '', title: ''};
	$scope.posts = Post.all;
	$scope.deletePost = function (postId) {
  		Post.delete(postId);
  	};
	$scope.user = Auth.user;
});
